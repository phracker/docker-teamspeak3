#!/usr/bin/env bash

set -e

function stop {
	kill $(ps -C ts3server -o pid= | awk '{ print $1; }')
	exit
}

trap stop INT
trap stop TERM


# create symlinks for all files and directories in the persistent data directory
# cd ${TS_DIRECTORY}
cd "${TS_DIRECTORY}/teamspeak3-server_linux_amd64/"
rm -rf ts3server.sqlitedb ts3server.sqlitedb-shm ts3server.sqlitedb-wal files logs ts3server.pid
if [ -e serverkey.dat ]; then
	mv serverkey.dat /data
fi



# remove broken symlinks
#find -L "${TS_DIRECTORY}/teamspeak3-server_linux_amd64/" -type l -delete

mkdir -p /data/files /data/logs

# create symlinks for static files
# STATIC_FILES=(
#   ts3server.sqlitedb
#   ts3server.sqlitedb-shm
#   ts3server.sqlitedb-wal
# )
# for i in ${STATIC_FILES[@]}
# do
#   ln -sf /data/${i}
# done

for i in $(ls /data)
do
	ln -sf /data/${i}
done

find -L "${TS_DIRECTORY}/teamspeak3-server_linux_amd64/" -type l -delete

export LD_LIBRARY_PATH=".:$LD_LIBRARY_PATH"
exec /tini -- ./ts3server $@
